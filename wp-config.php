<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'chambery_en_lutte' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@7E#fix}IT cuX!{-r~?=(fo,{=%fo5ode9&S4v9QE)I.zIr1>ii?B3sO:VbV._:' );
define( 'SECURE_AUTH_KEY',  '%&&a~3K9PC^]?,q<wslt=TZ.R-sf$T<T,vHGU40?}g?c.CN@|Rq$Lj+qh}%Qz+N.' );
define( 'LOGGED_IN_KEY',    'leWg ~NDF#z4Cy^oWa/1P>Zk<~M;Z6JxS^%%2X]G@N9&Y*{q6|xKQm/ljJm>vUCk' );
define( 'NONCE_KEY',        'drG26!WFD=!!DrWRe|EcaJS>*.RvQ1thX(VzDAT }3@cf02sA8pKcC.3KEMmBC}k' );
define( 'AUTH_SALT',        '1_)@Ig+;x0+o;|}OE1{v|0)VQ&8Lc.`WH`,x?$AN+GgSqicY2BPLig_r7h]ATU7~' );
define( 'SECURE_AUTH_SALT', 'd}0OY+Uh}>Q>h,-K=bI5?mImGQPcL|rp?<DA=+xLGY|O9>/QvYqt_s/98G3Ks~R~' );
define( 'LOGGED_IN_SALT',   '.?rZ:[,HrTdlL1VEW!ymAFs?*hR&oSg3tX]R[?367Z|I3aKQCs>[YFDC[YW>XrRx' );
define( 'NONCE_SALT',       'AEEjxv y?LiR:MHd![I(ziks_:%bQ&R@t..C>&JnEH]n<E$4(BTgeQX}Vc>|l#6;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
